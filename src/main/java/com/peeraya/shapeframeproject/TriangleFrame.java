/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeframeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class TriangleFrame extends JFrame {
    
    JLabel lblBase;
    JLabel lblHeight;
    JLabel lblA;
    JLabel lblB;
    JLabel lblC;
    JTextField txtBase;
    JTextField txtHeight;
    JTextField txtA;
    JTextField txtB;
    JTextField txtC;
    JButton btnCalculate;
    JLabel lblResult;
    JLabel lblResult2;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        // ความยาวฐาน
        lblBase = new JLabel("Base: ", JLabel.TRAILING);
        lblBase.setSize(45, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);
        
        // ความยาวด้านสูง
        lblHeight = new JLabel("Height: ", JLabel.TRAILING);
        lblHeight.setSize(45, 20);
        lblHeight.setLocation(5, 30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
       
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);

        // ด้าน a ตรงข้ามมุม A
        lblA = new JLabel("A: ", JLabel.TRAILING);
        lblA.setSize(45, 20);
        lblA.setLocation(5, 55);
        lblA.setBackground(Color.WHITE);
        lblA.setOpaque(true);
        this.add(lblA);
        
        txtA = new JTextField();
        txtA.setSize(50, 20);
        txtA.setLocation(60, 55);
        this.add(txtA);
        
        // ด้าน b ตรงข้ามมุม B
        lblB = new JLabel("B: ", JLabel.TRAILING);
        lblB.setSize(45, 20);
        lblB.setLocation(5, 80);
        lblB.setBackground(Color.WHITE);
        lblB.setOpaque(true);
        this.add(lblB);
       
        txtB = new JTextField();
        txtB.setSize(50, 20);
        txtB.setLocation(60, 80);
        this.add(txtB);
        
        // ด้าน c ตรงข้ามมุม C
        lblC = new JLabel("C: ", JLabel.TRAILING);
        lblC.setSize(45, 20);
        lblC.setLocation(5, 105);
        lblC.setBackground(Color.WHITE);
        lblC.setOpaque(true);
        this.add(lblC);
       
        txtC = new JTextField();
        txtC.setSize(50, 20);
        txtC.setLocation(60, 105);
        this.add(txtC);

        // ปุ่ม Calculate
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle Base = ???, Height = ???, A = ???, B = ???, C = ???");
        lblResult.setHorizontalAlignment(lblResult.CENTER);
        lblResult.setSize(385, 50);
        lblResult.setLocation(0, 150);
        lblResult.setForeground(Color.WHITE);
        lblResult.setBackground(Color.BLUE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        lblResult2 = new JLabel("Area = ???, Perimeter = ???");
        lblResult2.setHorizontalAlignment(lblResult2.CENTER);
        lblResult2.setSize(385, 50);
        lblResult2.setLocation(0, 185);
        lblResult2.setForeground(Color.WHITE);
        lblResult2.setBackground(Color.BLUE);
        lblResult2.setOpaque(true);
        this.add(lblResult2);

        // Event Driven: การเขียนโปรแกรมที่ตอบสนองตามเหตุการณ์
        btnCalculate.addActionListener(new ActionListener() { // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // 1. ดึงข้อมูลจาก 
                    // txtBase -> strBase
                    // txtHeight -> strHeight
                    // txtA -> strA
                    // txtB -> strB
                    // txtC -> strC
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    String strA = txtA.getText();
                    String strB = txtB.getText();
                    String strC = txtC.getText();
                    // 2. แปลง strBase -> base: double parseDouble
                    // strHeight -> height: double parseDouble
                    // strA -> a: double parseDouble
                    // strB -> b: double parseDouble
                    // strC -> c: double parseDouble
                    double a = Double.parseDouble(strA);
                    double b = Double.parseDouble(strB);
                    double c = Double.parseDouble(strC);
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    // 3. instance object Triangle(base, height, a, b, c) -> triangle
                    Triangle triangle = new Triangle(base, height, a, b, c);
                    // 4. update lblResult, lblResult2 โดยนำข้อมูลจาก triangle ไปแสดงให้ครบถ้วน
                    lblResult.setText("Triangle Base = " + triangle.getBase()
                            + ", Height = " + triangle.getHeight()
                            + ", A = " + triangle.getA()
                            + ", B = " + triangle.getB()
                            + ", C = " + triangle.getC());
                    lblResult2.setText("Area = " + String.format("%.2f", triangle.calArea())
                            + ", Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtA.setText("");
                    txtA.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
