/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeframeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class CircleFrame2 extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;

    public CircleFrame2() {
        super("Circle");
        this.setSize(325, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblRadius = new JLabel("radius: ", JLabel.TRAILING);
        lblRadius.setSize(45, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Circle radius = ???, Area = ???, Perimeter = ???");
        lblResult.setHorizontalAlignment(lblResult.CENTER);
        lblResult.setSize(310, 50);
        lblResult.setLocation(0, 50);
        lblResult.setForeground(Color.WHITE);
        lblResult.setBackground(Color.BLUE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        // Event Driven: การเขียนโปรแกรมที่ตอบสนองตามเหตุการณ์
        btnCalculate.addActionListener(new ActionListener() { // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // 1. ดึงข้อมูลจาก txtRadius -> strRadius
                    String strRadius = txtRadius.getText();
                    // 2. แปลง strRadius -> radius: double parseDouble
                    double radius = Double.parseDouble(strRadius);
                    // 3. instance object Circle(radius) -> circle
                    Circle circle = new Circle(radius);
                    // 4. update lblResult โดยนำข้อมูลจาก circle ไปแสดงให้ครบถ้วน
                    lblResult.setText("Circle radius = " + circle.getRadius()
                            + ", Area = " + String.format("%.2f", circle.calArea())
                            + ", Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame2.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        CircleFrame2 frame = new CircleFrame2();
        frame.setVisible(true);
    }
}
