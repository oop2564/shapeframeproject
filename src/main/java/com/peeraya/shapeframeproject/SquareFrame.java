/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeframeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class SquareFrame extends JFrame {

    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(325, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSide = new JLabel("side: ", JLabel.TRAILING);
        lblSide.setSize(45, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Square side = ???, Area = ???, Perimeter = ???");
        lblResult.setHorizontalAlignment(lblResult.CENTER);
        lblResult.setSize(310, 50);
        lblResult.setLocation(0, 50);
        lblResult.setForeground(Color.WHITE);
        lblResult.setBackground(Color.BLUE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        // Event Driven: การเขียนโปรแกรมที่ตอบสนองตามเหตุการณ์
        btnCalculate.addActionListener(new ActionListener() { // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // 1. ดึงข้อมูลจาก txtSide -> strSide
                    String strSide = txtSide.getText();
                    // 2. แปลง strSide -> side: double parseDouble
                    double side = Double.parseDouble(strSide);
                    // 3. instance object Square(side) -> square
                    Square square = new Square(side);
                    // 4. update lblResult โดยนำข้อมูลจาก square ไปแสดงให้ครบถ้วน
                    lblResult.setText("Square side = " + square.getSide()
                            + ", Area = " + String.format("%.2f", square.calArea())
                            + ", Perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}
