/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeframeproject;

/**
 *
 * @author Administrator
 */
public class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double calArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double calPerimeter() {
        return 0.5 * Math.PI * radius;
    }

}
