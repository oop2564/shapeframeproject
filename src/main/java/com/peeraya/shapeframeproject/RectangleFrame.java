/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeframeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class RectangleFrame extends JFrame{
    JLabel lblWidth;
    JLabel lblLength;
    JTextField txtWidth;
    JTextField txtLength;
    JButton btnCalculate;
    JLabel lblResult;
    JLabel lblResult2;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(325, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("width: ", JLabel.TRAILING);
        lblWidth.setSize(45, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
        
        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);
        
        lblLength = new JLabel("length: ", JLabel.TRAILING);
        lblLength.setSize(45, 20);
        lblLength.setLocation(5, 30);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        this.add(lblLength);
       
        txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 30);
        this.add(txtLength);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width = ???, length = ???");
        lblResult.setHorizontalAlignment(lblResult.CENTER);
        lblResult.setSize(310, 50);
        lblResult.setLocation(0, 80);
        lblResult.setForeground(Color.WHITE);
        lblResult.setBackground(Color.BLUE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        lblResult2 = new JLabel("Area = ???, Perimeter = ???");
        lblResult2.setHorizontalAlignment(lblResult2.CENTER);
        lblResult2.setSize(310, 50);
        lblResult2.setLocation(0, 110);
        lblResult2.setForeground(Color.WHITE);
        lblResult2.setBackground(Color.BLUE);
        lblResult2.setOpaque(true);
        this.add(lblResult2);

        // Event Driven: การเขียนโปรแกรมที่ตอบสนองตามเหตุการณ์
        btnCalculate.addActionListener(new ActionListener() { // Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // 1. ดึงข้อมูลจาก txtWidth -> strWidth
                    // txtLength -> strLength
                    String strWidth = txtWidth.getText();
                    String strLength = txtLength.getText();
                    // 2. แปลง strWidth -> width: double parseDouble
                    // strLength -> length: double parseDouble
                    double width = Double.parseDouble(strWidth);
                    double length = Double.parseDouble(strLength);
                    // 3. instance object Rectangle(width, length) -> rectangle
                    Rectangle rectangle = new Rectangle(width, length);
                    // 4. update lblResult, lblResult2 โดยนำข้อมูลจาก rectangle ไปแสดงให้ครบถ้วน
                    lblResult.setText("Rectangle width = " + rectangle.getWidth() + 
                            ", length = " + rectangle.getLength());
                    lblResult2.setText("Area = " + String.format("%.2f", rectangle.calArea())
                            + ", Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
